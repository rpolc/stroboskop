# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git pull
```

Naloga 6.2.3:
((https://bitbucket.org/rpolc/stroboskop/commits/511a6e78c5e7df6a4fbd30275c37799e32e96f19))

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
((https://bitbucket.org/rpolc/stroboskop/commits/2845c7d6fdb8b308a64e2489db6b305943d99920))

Naloga 6.3.2:
((https://bitbucket.org/rpolc/stroboskop/commits/76df1cf06e0d5d724f009f4515f36643ed865890))

Naloga 6.3.3:
((https://bitbucket.org/rpolc/stroboskop/commits/527963fbdc2ef420311047453ce0c5a6aa526a50))

Naloga 6.3.4:
((https://bitbucket.org/rpolc/stroboskop/commits/80453bfb6c873a8900ca50a5a2d3d1712d68677d))

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
((https://bitbucket.org/rpolc/stroboskop/commits/2e8864e4dff32b3af751fc45ae18d9528d3a62b5))

Naloga 6.4.2:
((https://bitbucket.org/rpolc/stroboskop/commits/780badf58110c852e11b63277eb7f41fd03e3e0c))

Naloga 6.4.3:
((https://bitbucket.org/rpolc/stroboskop/commits/be967ae5cfc0a20b23db695685fd3c100cfa1789))

Naloga 6.4.4:
((https://bitbucket.org/rpolc/stroboskop/commits/3e8bb1cf053f69e614d0038cdc181a4acfbc901b))